﻿/*
    Game board
    
    Input: start cell
    Output: true if finish cell 'F' can be reached
    
    Traversal rules:
        1. Can only travel up,down,left,right
        2. Can only travel to 'O' cells ('X' cell cannot be traversed)
  
    e.g.
    X O O S O
    O X X X O
    O O O X X
    O X O X O
    F O X X O
  
*/

// Note: another improvement is pre-processing all cells 
using System;

class Game
{

    int[][] m;
    Boolean?[][] memo; // initially null values

    public Game() { }

    bool canReachFinish(int i, int j)
    {
        //if (memo[i][j] != null) return memo[i][j];
        bool[][] visited = new bool[10][];//matrix[0].Length
        bool found = search(i, j, visited);
        // note: can also mark all visited nodes in memo array
        memo[i][j] = found;
        return found;
    }

    bool search(int i, int j, bool[][] v)
    {
        if (i < 0 || j < 0 || i >= m.Length || j >= m[0].Length || m[i][j] == 'X' || v[i][j]) return false;
        v[i][j] = true;
        if (memo[i][j] != null) return (bool)memo[i][j];
        if (m[i][j] == 'F') return true;
        return search(i - 1, j, v) || search(i + 1, j, v) || search(i, j - 1, v) || search(i, j + 1, v);
    }

}
