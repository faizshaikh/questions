﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace taxicabnumbers
{
    class Program
    {
        static Stopwatch t = new Stopwatch();
        ///http://rosettacode.org/wiki/Taxicab_numbers
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the nth taxicab number you are looking");
            int n = Int32.Parse( Console.ReadLine());
            
            t.Start();
                var a= GetTaxicabNumberatPosition(n);

            Console.WriteLine(n + " th ramanujan number is " + a.Item1);
            Console.WriteLine( " Pairs are {0}:{1},\t{2}:{3}" , a.Item2[0].Item1, a.Item2[0].Item2, a.Item2[1].Item1, a.Item2[1].Item2);
            Console.WriteLine("Checking for {0} took {1}:{2}", n, t.Elapsed.Minutes, t.Elapsed.Seconds);
            Console.ReadKey();
        }

        private static Tuple<long, IList<Tuple<int, int>> > GetTaxicabNumberatPosition(int pos)
        {
            SortedList<long, IList<Tuple<int, int>>> sumsOfTwoCubes = new SortedList<long, IList<Tuple<int, int>>>();
            var pointer = 0;
            long sum = 0;
            {
                for (int i = 1; i < int.MaxValue; i++)
                {
                    for (int j = 1; j < int.MaxValue; j++)
                    {
                        sum = (long)(Math.Pow((double)i, 3) + Math.Pow((double)j, 3));

                        if (!sumsOfTwoCubes.ContainsKey(sum))
                        {
                            sumsOfTwoCubes.Add(sum, new List<Tuple<int, int>> {new Tuple<int, int>( i, j )});

                        }
                        else
                        {
                            //Sum is already present so check for the index
                            sumsOfTwoCubes[sum].Add(new Tuple<int, int>(i, j));

                            if (++pointer == pos)
                                return new Tuple<long, IList<Tuple<int, int>>>(sum, sumsOfTwoCubes[sum]);
                        }
                        if (j >= i)
                        {
                            break;
                        }
                    }
                }
            }
            return null;
        }
    }
}
