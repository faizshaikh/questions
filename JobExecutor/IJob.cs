﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobExecutor
{
    public interface IJob
    {
         bool Execute();
    }
}
