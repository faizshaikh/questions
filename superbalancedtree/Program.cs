﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace superbalancedtree
{
    class Program
    {
        static void Main(string[] args)
        {
        }

    }

    public class BinaryTreeNode
    {

        public int value;
        public BinaryTreeNode left;
        public BinaryTreeNode right;

        public BinaryTreeNode(int value)
        {
            this.value = value;
        }

        public BinaryTreeNode insertLeft(int leftValue)
        {
            this.left = new BinaryTreeNode(leftValue);
            return this.left;
        }

        public BinaryTreeNode insertRight(int rightValue)
        {
            this.right = new BinaryTreeNode(rightValue);
            return this.right;
        }
        public bool isBalanced(BinaryTreeNode treeRoot)
        {
            List<int> depths = new List<int>(); // we short-circuit as soon as we find more than 2

            // nodes will store pairs of a node and the node's depth
            Stack<NodeDepthPair> nodes = new Stack<NodeDepthPair>();
            nodes.Push(new NodeDepthPair(treeRoot, 0));

            while (nodes.Any())
            {

                // pop a node and its depth from the top of our stack
                NodeDepthPair nodeDepthPair = nodes.Pop();
                BinaryTreeNode node = nodeDepthPair.node;
                int depth = nodeDepthPair.depth;

                // case: we found a leaf
                if (node.left == null && node.right == null)
                {

                    // we only care if it's a new depth
                    if (!depths.Contains(depth))
                    {
                        depths.Add(depth);

                        // two ways we might now have an unbalanced tree:
                        //   1) more than 2 different leaf depths
                        //   2) 2 leaf depths that are more than 1 apart
                        if ((depths.Count > 2) ||
                                (depths.Count == 2 && Math.Abs(depths[0] - depths[1]) > 1))
                        {
                            return false;
                        }
                    }

                    // case: this isn't a leaf - keep stepping down
                }
                else
                {
                    if (node.left != null)
                    {
                        nodes.Push(new NodeDepthPair(node.left, depth + 1));
                    }
                    if (node.right != null)
                    {
                        nodes.Push(new NodeDepthPair(node.right, depth + 1));
                    }
                }
            }

            return true;
        }

    }

    public class NodeDepthPair
    {

        public BinaryTreeNode node;
        public int depth;

        public NodeDepthPair(BinaryTreeNode node, int depth)
        {
            this.node = node;
            this.depth = depth;
        }
    }

}
