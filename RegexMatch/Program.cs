﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegexMatch
{
    class Program
    {
        static void Main(string[] args)
        {
        }
        //https://www.experts-exchange.com/questions/23815649/C-String-matching-using-wildcards.html
        public static bool Match(string strWithWildCards, string myString)
        {
            if (strWithWildCards.Length == 0) return myString.Length == 0;
            if (myString.Length == 0) return false;
            if (strWithWildCards[0] == '*' && strWithWildCards.Length > 1)
                for (int index = 0; index < myString.Length; index++)
                {
                    if (Match(strWithWildCards.Substring(1), myString.Substring(index)))
                        return true;
                }
            else if (strWithWildCards[0] == '*')
                return true;
            else if (strWithWildCards[0] == myString[0])
                return Match(strWithWildCards.Substring(1), myString.Substring(1));
            return false;
        }
    }

}
}
