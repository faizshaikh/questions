﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxStack
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }
    public class MaxStack
    {

        Stack<int> stack = new Stack<int>();
        Stack<int> maxsStack = new Stack<int>();

        // Add a new item to the top of our stack. If the item is greater
        // than or equal to the the last item in maxsStack, it's
        // the new max! So we'll add it to maxsStack.
        public int Push(int item)
        {
            stack.Push(item);
            if (!maxsStack.Any() || item >= (int)maxsStack.Peek())
            {
                maxsStack.Push(item);
            }
            return item;
        }

        // Remove and return the top item from our stack. If it equals
        // the top item in maxsStack, they must have been pushed in together.
        // So we'll Pop it out of maxsStack too.
        public int Pop()
        {
            int item = (int)stack.Pop();
            if (item == (int)maxsStack.Peek())
            {
                maxsStack.Pop();
            }
            return item;
        }

        // The last item in maxsStack is the max item in our stack.
        public int getMax()
        {
            return (int)maxsStack.Peek();
        }
    }

}
