﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace moveZeroesToEndOArray
{
    class Program
    {
        static void Main(string[] args)
        {
        }
        public static void moveZeroes(int[] a)
        {
            int pos = 0;
            for (int i = 0; i < a.Length; i++)
                if (a[i] != 0)
                    a[pos++] = a[i];
            for (int i = pos; i < a.Length; i++)
                a[i] = 0;
        }
    }
}
