﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public abstract class Node
    {
        public abstract double evaluate();
    }
    public abstract class OpNode: Node
    {
        public Node leftNode;
        public Node rightNode;
        public abstract override double evaluate();
        
    }
    public abstract class AdditionNode : OpNode
    {
        public override double evaluate()
        {
            return leftNode.evaluate() + rightNode.evaluate();
        }

    }

    public abstract class MultiplicationNode : OpNode
    {

        public override double evaluate()
        {
            return leftNode.evaluate() * rightNode.evaluate();
        }
    }

    public class ValueNode : Node
    {
        public double val;
        public override double evaluate()
        {
            return val;
        }
    }
}
