﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountingSort
{
    class Program
    {
        static void Main(string[] args)
        {
        }

        /*
         Counting sort is a very time-efficient (and somewhat space-inefficient) algorithm for sorting that avoids comparisons and exploits the O(1)O(1) time insertions and lookups in an array.

        The idea is simple: if you're sorting  integers and you know they all fall in the range 1..1001..100, you can generate a sorted array this way:

        Allocate an array numsToCounts where the indices represent numbers from our input array and the values represent how many times the index number appears. Start each value at 0.
        In one pass of the input array, update numsToCounts as you go, so that at the end the values in numsToCounts are correct.
        Allocate an array sortedArray where we'll store our sorted numbers.
        In one in-order pass of numsToCounts put each number, the correct number of times, into sortedArray.

             */
        public int[] countingSort(int[] theArray, int maxValue)
        {

            // array of 0s at indices 0..maxValue
            int[] numsToCounts = new int[maxValue + 1];

            // populate numsToCounts
            foreach (int num in theArray)
            {
                numsToCounts[num] += 1;
            }

            // populate the final sorted array
            int[] sortedArray = new int[theArray.Length];
            int currentSortedIndex = 0;

            // for each num in numsToCounts
            for (int num = 0; num < numsToCounts.Length; num++)
            {
                int count = numsToCounts[num];

                // for the number of times the item occurs
                for (int x = 0; x < count; x++)
                {

                    // add it to the sorted array
                    sortedArray[currentSortedIndex] = num;
                    currentSortedIndex++;
                }
            }

            return sortedArray;
        }

    }
}
