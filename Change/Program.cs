﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Change
{
    class Program
    {
        static void Main(string[] args)
        {
        }
        /*
         *   int[] waysOfDoingNCents1 = new int[]{
    1,  // 0c:  no coins
    1,  // 1c:  1 1c coin
    1,  // 2c:  2 1c coins
    1,  // 3c:  3 1c coins
    1,  // 4c:  4 1c coins
    1,  // 5c:  5 1c coins
}

Now what if we add a 2¢ coin?

  int[] waysOfDoingNCents1And2 = new int[]{
    1,    // 0c:  no change
    1,    // 1c:  no change
    1+1,  // 2c:  new [(2)]
    1+1,  // 3c:  new [(2,1)]
    1+2,  // 4c:  new [(2,1,1), (2,2)]
    1+2,  // 5c:  new [(2,1,1,1), (2,2,1)]
}s
         * */
        public int changePossibilitiesBottomUp(int amount, int[] denominations)
        {
            int[] waysOfDoingNCents = new int[amount + 1]; // array of zeros from 0..amount
            waysOfDoingNCents[0] = 1;

            foreach (int coin in denominations)
            {
                for (int higherAmount = coin; higherAmount < amount + 1; higherAmount++)
                {
                    int higherAmountRemainder = higherAmount - coin;
                    waysOfDoingNCents[higherAmount] += waysOfDoingNCents[higherAmountRemainder];
                }
            }

            return waysOfDoingNCents[amount];
        }

    }
}
