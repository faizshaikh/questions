﻿using System;

namespace randomgenerators
{
    class Program
    {
        const UInt32 startA = 65;
        const UInt32 startB = 8921;
        private static UIntPtr stateA;
        private static UIntPtr stateB;

        static void Main(string[] args)
        {
            stateA = (UIntPtr)startA;
            stateB = (UIntPtr)startB;
            int count = 0;
            for (int i =0; i< 40000000; i++)
            {
                var a = generatorA();
                var b = generatorB();
                //Console.WriteLine($"{i+1} : {a}\t\t{b}");
                //Console.WriteLine($"{Convert.ToString((long) ((UInt64)a),2)}");
                //Console.WriteLine($"{Convert.ToString((long)((UInt64)b), 2)}");
                //Console.WriteLine($"{Convert.ToString((long)((UInt64)(((UInt64)a ^ (UInt64)b) )), 2)}");

                if ((((UInt64)a ^ (UInt64)b) & 0x0000FFFF)==0 )
                {
                    Console.WriteLine($"{++count} : {a}\t\t{b}");
                }
                //Console.WriteLine();
            }
            Console.WriteLine($"{count} pairs found");
        }
        
        private static UIntPtr generatorA()
        {
            return stateA = (UIntPtr)(((UInt64)stateA * (UInt64)16807) % 2147483647); 
        }
        private static UIntPtr generatorB()
        {
            return stateB = (UIntPtr)(((UInt64)stateB * (UInt64)48271) % 2147483647);
        }

    }
}
