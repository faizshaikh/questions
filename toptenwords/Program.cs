﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace toptenwords
{
    class Program
    {
        private static string wordsstring = "hello how r u u u u  u  u u  u  u u u  u u u u  u u u ? hello there u u u u ! great to c u there. hello .hello hello hello hello hello .hello hello hello hello hello hello ";
        private static string[] words = "hello how r u u u u  u  u u  u  u u u  u u u u  u u u ? hello there u u u u ! great to c u there. hello .hello hello hello hello hello .hello hello hello hello hello hello ".Split(" ".ToCharArray());
        private static long index = 0;
        private static TrieNode root = new TrieNode(null, '?');

        static void Main(string[] args)
        {
            var a = GetNextWord();
            //List<WordCounts> =new List<WordCounts>();
            Hashtable ht = new Hashtable();
            int [] max = new int[10];
            string [] topwords = new string[10];
            //var List



            while (a != null)
            {
                root.AddWord(a);
                a = GetNextWord();
            }
            List<TrieNode> top10_nodes = new List<TrieNode> { root, root, root, root, root, root, root, root, root, root };
            int distinct_word_count = 0;
            int total_word_count = 0;
            root.GetTopCounts(ref top10_nodes, ref distinct_word_count, ref total_word_count);
            top10_nodes.Reverse();
            foreach (TrieNode node in top10_nodes)
            {
                Console.WriteLine("{0} - {1} times", node.ToString(), node.m_word_count);
            }

            Console.WriteLine();
            Console.WriteLine("{0} words counted", total_word_count);
            Console.WriteLine("{0} distinct words found", distinct_word_count);
            Console.WriteLine();
            Console.WriteLine("done.");
            Console.ReadKey();

        }

        private static string GetNextWord()
        {
            try
            {
                return words[index++];
            }
            catch {
                return null;
            }
        }
        private static void Reset()
        {
            index = 0;
        }
        private static  void LinqSolution()
        {
            //http://stackoverflow.com/questions/4495241/given-a-file-find-the-ten-most-frequently-occuring-words-as-efficiently-as-poss
            int wordFrequency = 10;


            var result = (from word in wordsstring.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries)
                          group word by word into g
                          select new { Word = g.Key, Occurrence = g.Count() }).ToList().FindAll(i => i.Occurrence >= wordFrequency);

            //return result;
        }
    }
}
