﻿using System;

namespace BinarySearchTreeChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
        public bool IsBinarySearchTree(BinaryTreeNode root)
        {
            return IsBinarySearchTree(root, int.MinValue, int.MaxValue);
        }

        public bool IsBinarySearchTree(BinaryTreeNode root, int lowerBound, int upperBound)
        {
            if (root == null)
            {
                return true;
            }

            if (root.Value >= upperBound || root.Value <= lowerBound)
            {
                return false;
            }

            return IsBinarySearchTree(root.Left, lowerBound, root.Value)
                   && IsBinarySearchTree(root.Right, root.Value, upperBound);
        }
    }
    public class BinaryTreeNode
    {
        public int Value { get; }

        public BinaryTreeNode Left { get; private set; }

        public BinaryTreeNode Right { get; private set; }

        public BinaryTreeNode(int value)
        {
            Value = value;
        }

        public BinaryTreeNode InsertLeft(int leftValue)
        {
            Left = new BinaryTreeNode(leftValue);
            return Left;
        }

        public BinaryTreeNode InsertRight(int rightValue)
        {
            Right = new BinaryTreeNode(rightValue);
            return Right;
        }
    }

}
