﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace secondlargest
{

    /*
     *
.        ( 5 )
        /     \
      (3)     (8)
     /  \     /  \
   (1)  (4) (7)  (9)
But what if the largest element itself has a left subtree?

.        ( 5 )
        /     \
      (3)     (8)
     /  \     /  \
   (1)  (4) (7)  (12)
                 /
               (10)
               /  \
             (9)  (11)
     * */
    public class BinaryTreeNode
    {

        public int value;
        public BinaryTreeNode left;
        public BinaryTreeNode right;

        public BinaryTreeNode(int value)
        {
            this.value = value;
        }

        public BinaryTreeNode insertLeft(int leftValue)
        {
            this.left = new BinaryTreeNode(leftValue);
            return this.left;
        }

        public BinaryTreeNode insertRight(int rightValue)
        {
            this.right = new BinaryTreeNode(rightValue);
            return this.right;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
        }

        public int findLargest(BinaryTreeNode rootNode)
        {
            BinaryTreeNode current = rootNode;
            while (current.right != null)
            {
                current = current.right;
            }
            return current.value;
        }

        public int findSecondLargest(BinaryTreeNode rootNode)
        {
            if (rootNode.left == null && rootNode.right == null)
            {
                throw new Exception("Tree must have at least 2 nodes");
            }

            BinaryTreeNode current = rootNode;

            while (true)
            {
                // case: current is largest and has a left subtree
                // 2nd largest is the largest in that subtree
                if (current.left != null && current.right == null)
                {
                    return findLargest(current.left);
                }

                // case: current is parent of largest, and
                // largest has no children, so
                // current is 2nd largest
                if (current.right != null &&
                        current.right.left == null &&
                        current.right.right == null)
                {
                    return current.value;
                }

                current = current.right;
            }
        }

    }

}
