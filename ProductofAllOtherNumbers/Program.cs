﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductofAllOtherNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] a = new int[] { 1,2,3 };
            Console.WriteLine("Input Array");
            PrintArray(a);
            multiply(a, 1, 0);
            Console.WriteLine("Output Array");
            PrintArray(a);
            Console.ReadKey();
        }

        //http://stackoverflow.com/questions/2680548/given-an-array-of-numbers-return-array-of-products-of-all-other-numbers-no-div
        static int multiply(int[] a, int revProduct, int indx)
        {
            int fwdProduct = 1;
            if (indx < a.Length)
            {
                fwdProduct = multiply(a, revProduct * a[indx], indx + 1);
                int cur = a[indx];

                a[indx] = revProduct * fwdProduct;

                fwdProduct *= cur;
            }
            return fwdProduct;
        }

        static int multiply2(int[] a, int revProduct, int indx)
        {
            int fwdProduct = 1;
            if (indx < a.Length)
            {
                fwdProduct = multiply2(a, revProduct * a[indx], indx + 1);
                int cur = a[indx];
                a[indx] = revProduct * fwdProduct;
                fwdProduct *= cur;
            }
            return fwdProduct;
        }

        static void PrintArray(int[] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine("[{0}] = {1}", i, a[i]);
            }
        }
    }
}
