﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShortestPathBetweenNodes
{
    class Program
    {
        static void Main(string[] args)
        {
            //Node n11, n2;
            Node n11 = new NodeFinder(Guid.NewGuid(), "N11");
            Node n12 = new NodeFinder(Guid.NewGuid(), "N12");
            Node n13 = new NodeFinder(Guid.NewGuid(), "N13");
            Node n14 = new NodeFinder(Guid.NewGuid(), "N14");
            Node n15 = new NodeFinder(Guid.NewGuid(), "N15");
            Node n16 = new NodeFinder(Guid.NewGuid(), "N16");
            Node n17 = new NodeFinder(Guid.NewGuid(), "N17");
            Node n18 = new NodeFinder(Guid.NewGuid(), "N18");
            Node n19 = new NodeFinder(Guid.NewGuid(), "N19");
            Node n20 = new NodeFinder(Guid.NewGuid(), "N20");

            Node n21 = new NodeFinder(Guid.NewGuid(), "N21");
            Node n22 = new NodeFinder(Guid.NewGuid(), "N22");
            Node n23 = new NodeFinder(Guid.NewGuid(), "N23");
            Node n24 = new NodeFinder(Guid.NewGuid(), "N24");
            Node n25 = new NodeFinder(Guid.NewGuid(), "N25");
            Node n26 = new NodeFinder(Guid.NewGuid(), "N26");
            Node n27 = new NodeFinder(Guid.NewGuid(), "N27");
            Node n28 = new NodeFinder(Guid.NewGuid(), "N28");
            Node n29 = new NodeFinder(Guid.NewGuid(), "N29");
            Node n30 = new NodeFinder(Guid.NewGuid(), "N30");

            ConnectNodes(n11, n21);
            ConnectNodes(n11, n12);
            //ConnectNodes(n11, n13);
            //ConnectNodes(n11, n14);
            //ConnectNodes(n11, n15);
            //ConnectNodes(n11, n16);

            //ConnectNodes(n12, n13);
            //ConnectNodes(n12, n14);
            //ConnectNodes(n12, n15);
            //ConnectNodes(n12, n16);
            //ConnectNodes(n12, n17);


            //ConnectNodes(n13, n14);
            //ConnectNodes(n13, n15);
            //ConnectNodes(n13, n16);
            //ConnectNodes(n13, n17);
            //ConnectNodes(n13, n18);


            //ConnectNodes(n14, n15);
            //ConnectNodes(n14, n16);
            //ConnectNodes(n14, n17);
            //ConnectNodes(n14, n18);
            //ConnectNodes(n14, n19);

            //ConnectNodes(n15, n15);
            //ConnectNodes(n15, n16);
            //ConnectNodes(n15, n17);
            //ConnectNodes(n15, n18);
            //ConnectNodes(n15, n19);

            ConnectNodes(n30, n12);
            //ConnectNodes(n30, n16);
            //ConnectNodes(n30, n17);
            //ConnectNodes(n30, n18);
            //ConnectNodes(n30, n19);

            var Path = FindShortestPath(n11, n30);
            foreach (var node in Path)
            {
                Console.WriteLine(node.Name);
            }
        }

        private static void ConnectNodes(Node n1, Node n2)
        {
            if (n1 != n2)
            {
                if (!n1.Connections.Contains(n2))
                    n1.Connections.Add(n2);
                if (!n2.Connections.Contains(n1))
                    n2.Connections.Add(n1);
            }
        }

        private static IList<Node> FindShortestPath(Node start, Node end)
        {
            if (start == end)
                return new List<Node>() { start, end };

            Queue<NodeFinder> q = new Queue<NodeFinder>();
            NodeFinder head = new NodeFinder(start);
            head.Visited = true;
            head.Path.Add(start);
            q.Enqueue(head);
            while (q.Count > 0)
            {
                NodeFinder n = q.Dequeue();
                foreach (NodeFinder nConnection in n.Connections)
                {
                    if (!nConnection.Visited && nConnection != end)
                    {
                            nConnection.Path = n.Path;
                            if (!n.Visited)
                                nConnection.Path.Add(n);

                            q.Enqueue(nConnection);
                    }
                    else {
                        n.Path.Add(n);
                        return n.Path;
                    }

                }
                n.Visited = true;
            }
            throw new Exception("Path not found");
        }
    }

    public abstract class Node
    {
        public HashSet<Node> Connections;
        public Guid ID;
        public string Name;
        public Node(Guid id, string name) { ID = id; Name = name; Connections = new HashSet<Node>(); }
    }

    public class NodeFinder : Node
    {
        public bool Visited;
        public IList<Node> Path;
        public NodeFinder(Guid id, string name) : base(id, name) { }
        public NodeFinder(Node n) : base(n.ID, n.Name)
        {
            base.Connections = n.Connections;
            Path = new List<Node>();
        }
    }
}
