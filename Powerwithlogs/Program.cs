﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Powerwithlogs
{
    class Program
    {
        //https://careercup.com/question?id=5733237978562560
        static void Main(string[] args)
        {
            var x = 2;
            var n = 5;
            Console.WriteLine($"{x} power {n} is {pow(x, n)}");
        }
        static double pow(int x, int n)
        {
            var bin = new BitArray(BitConverter.GetBytes(n));
            var logN = Convert.ToInt32( Math.Ceiling(Math.Log(n)));
            var pow = 1;
            var exp = x;
            for (var i = 0; i <= logN; i++)
            {
                if (bin[logN - i])
                {
                    pow *= exp;
                }
                exp *= exp;
            }
            return pow;
        }
    }
}
