﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintLargeNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 3;
            byte[] a = new byte[i];// Enumerable
              //.Repeat(0x0000, i)
              //.ToArray();
            PrintAll(a);
        }

        private static void generate(int prefix, int start, int n)
        {
            if (n == 0)
            {
                //Console.Write();
                Console.WriteLine(prefix);
            }
            else
            {
                for (int i = start; i < 10; i++)
                    generate(10 * prefix + i, i + 1, n - 1);
            }
        }


        private static void PrintAll(byte[] a)
        {
            while (!AddOne(a))
                Print(a);
        }

        private static void Print(byte[] a)
        {
            //Console.WriteLine(string.Join(String.Empty, a));
            //Console.WriteLine(string.Join(String.Empty, a));
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i].ToString("x"));
            }
            Console.WriteLine();
        }

        private static bool AddOne(byte[] a)
        {
            bool carry = false;
            int i = a.Length - 1;
            do
            {
                a[i] += 1;
                if (a[i] > 15) //9
                {
                    a[i] %= 16; //10
                    carry = true;
                }
                else
                {
                    carry = false;
                }
            }
            while (--i >= 0 && carry);

            return carry;
        }
    }
}
