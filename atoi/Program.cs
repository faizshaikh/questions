﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace atoi
{
    class Program
    {
        static void Main(string[] args)
        {
            string i = "zxcxz-4534";
            Console.WriteLine("my_atoi({0}):{1}" ,i, my_atoi(i));
            Console.ReadKey();
        }
        //https://www.careercup.com/question?id=7837664

        static int my_atoi(string str)
        {

            bool negative = false;
            int value = 0;
            int index = 0;

            if (str == null) /* if data passed in is null */
                return 0;

            
            while ((str[index]== ' ' || str[index] <= '0' || str[index] >= '9') && str[index] != '-') /* ignore pre-whitespaces */
                index++;

            if (str[index]  == '-')
            {
                negative = true;
                index++;
            }

            while (index <str.Length)
            {
                if (str[index] >= '0' && str[index] <= '9')
                { /* numeric characters */
                    if (!negative && value * 10 + ((int)str[index] - '0') < value)
                    { /* check for + buffer overflow */
                        Console.WriteLine ("buffer overflow \n");
                        return 0;
                    }
                    if (negative && (((-value) * 10) - ((int)str[index] - '0') > -value))
                    { /* check for - buffer overflow */
                        Console.WriteLine("buffer overflow \n");
                        return 0;
                    }
                    value = value * 10 + ((int)str[index] - '0');
                }

                index++;
            }

            if (negative)
                value = -value;

            return value;
        }


    }
}
